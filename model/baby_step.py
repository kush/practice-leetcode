from dataclasses import dataclass

@dataclass
class BabyStep:
    first_number: int = 1
    second_number: int = 2

    def add_two_numbers(self, first_number: int = 1, second_number: int = 2) -> int:
        """

        :param first_number:
        :param second_number:
        :return: this is the sum of the two numbers
        """
        return first_number + second_number

    def add_two_positive_numbers(self, first_number: int = 1, second_number: int = 2) -> int:
        """
        Add two positive numbers
        If either number is zero or negative, return zero
        else, return the sum
        This function demonstrates if condition
        :param first_number:
        :param second_number:
        :return: integer
        """
        if first_number <= 0 or second_number <= 0:
            return 0
        else:
            return first_number + second_number

    def find_the_smallest_number(self, number_array: [int] = None) -> int:
        """
        Return the smallest number in an array
        This function demonstrates for loops
        :param number_array:
        :return: integer
        """
        if number_array is None:
            number_array = [1, 2, 3, 4, 5]
        guess = number_array[0]
        for number in number_array:
            if guess > number:
                guess = number
        return guess

    def get_fibonacci_numbers(self, limit: int):
        """
        Return a list of Fibonacci numbers up to the limit
        :param limit: return no Fibonacci number greater than this
        :return: int
        """
        result = []
        a, b = 0, 1
        while a < limit:
            result.append(a)
            a, b = b, a + b
        return result
