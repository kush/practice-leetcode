import unittest


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        pass

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
