import unittest

from model.baby_step import BabyStep


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.baby_step = BabyStep(5, 7)

    def test_add_two_numbers(self):
        self.assertEqual(self.baby_step.add_two_numbers(first_number=self.baby_step.first_number,
                                                        second_number=self.baby_step.second_number), 12,
                         "add 2 and 3")

    def test_add_two_positive_numbers_true(self):
        self.assertEqual(self.baby_step.add_two_positive_numbers(self.baby_step.first_number,
                                                                 self.baby_step.second_number), 12,
                         "the sum of two and three is five")

    def test_add_two_positive_numbers_false(self):
        self.assertEqual(BabyStep.add_two_positive_numbers(self.baby_step, self.baby_step.first_number, 0), 0,
                         "we return zero if either of the two number isn't positive")

    def test_find_the_smallest_number(self):
        self.assertEqual(self.baby_step.find_the_smallest_number([6, 5, 4, 3, 33]), 3,
                         "three is the smallest number here")

    def test_get_fibonacci_numbers(self):
        limit = 100
        expected_output = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
        self.assertEqual(self.baby_step.get_fibonacci_numbers(100), expected_output,
                         "Add two numbers to get the next number")

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
